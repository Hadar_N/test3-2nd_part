export const env = (key: string):string => {
    const value = process.env[key];
    if(!value) throw new Error(`Missing: process.env['${key}'].`);
    return value;
}

//COPY FROM SANDBOX (previous task):

// your answer here...

export interface Place {
  name: string;
  hits: number;
  agent: number;
}

enum AgentType {
  Human,
  Android,
  Sensor,
  Machine
}

export interface Agent {
  name: string;
  type: AgentType;
  owner?: string;
  status: "pending" | "active" | "retired";
  places: Place[];
  // addPlace(newPlace: Place): void;
}
export interface AgentInput {
  name: string;
  type: string;
  owner?: string;
  status: string;
}

const reporter: Agent = {
  name: "John Doe",
  type: AgentType.Human,
  status: "active",
  places: [],
  // addPlace: (newPlace: Place): void => {
  //   reporter.places.push(newPlace);
  // }
};


// const getAgentInfo: Function = (ag: Agent): string => {
//   let placesString = JSON.stringify(ag.places);
//   placesString = placesString.substring(1, placesString.length - 1);
//   placesString = placesString.split(`"`).join(` `);
//   return `${ag.name} is a ${
//     AgentType[ag.type]
//   } agent, active in the following places: ${placesString}`;
// };
// console.log(getAgentInfo(reporter));
