import log from "@ajar/marker";
import { connection as db } from "../../db/mysql-connection";
import { OkPacket, RowDataPacket } from "mysql2/promise";

import {Agent,Place} from "../../utils/index";

// CREATES A NEW AGENT
export const create_agent = async ( ag: Agent ): Promise<number> => {

  const sql = `INSERT INTO agent SET ?`;
  const results: OkPacket = (await db.query(sql, ag))[0] as OkPacket;

  return(results.affectedRows)
};

// CREATES A NEW PLACE
export const create_place = async ( pl: Place ): Promise<number> => {

  const sql = `INSERT INTO place SET ?`;
  const results: OkPacket = (await db.query(sql, pl))[0] as OkPacket;

  return(results.affectedRows)
};

// GET ALL AGENTS
export const get_all_agents = async (): Promise<Agent[]> => {
  const sql = `
  SELECT *
  FROM agent`;

  const results : Agent[] = (await db.query(sql))[0] as Agent[];

  return results ;
};

// GET A SINGLE AGENT
export const get_agent_by_id = async (ag_id: string): Promise<Agent | null> => {
  const sql = `SELECT * FROM agent WHERE id = '${ag_id}'`;
  const results: any[] = (await db.query(sql))[0] as any[];
  if(!results.length) return null;
  return results[0] as Agent;
};

