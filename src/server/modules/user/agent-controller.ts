/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route-async-wrapper";
import express, { Request,Response } from "express";
import log from "@ajar/marker";

import { validate_place,validate_agent } from "./agent-type-validate";
import { Agent, Place} from "../../utils/index";

import {
  get_all_agents,
  get_agent_by_id,
  create_agent,
  create_place
} from './agent-service';

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW AGENT
router.post("/",raw(async (req:Request, res:Response) => {

    log.obj(req.body, "create a user, req.body:");

    const valid = validate_agent(req.body);
    if(!valid) throw new Error("input not valid");

    const ok = {status:200,message:`Agent created successfully`};
    const fail = {status:404,message:`Error in creating agent`};

    const res_affected_rows = await create_agent(req.body as Agent);
    const {status,message} = res_affected_rows ? ok : fail;

    res.status(status).json({message});

  })
);

// CREATES A NEW PLACE
router.post("/place",raw(async (req:Request, res:Response) => {

    log.obj(req.body, "create a place, req.body:");

    const valid = validate_place(req.body);
    if(!valid) throw new Error("input not valid");

    const ok = {status:200,message:`Place created successfully`};
    const fail = {status:404,message:`Error in creating place`};

    const res_affected_rows = await create_place(req.body as Place);
    const {status,message} = res_affected_rows ? ok : fail;

    res.status(status).json({message});

  })
);

// GET ALL AGENT
router.get("/",raw(async ( _ , res:Response) => {
    const users: Agent[] = await get_all_agents();
    res.status(200).json(users);
  })
);


// GETS A SINGLE AGENT
router.get("/:id",raw(async (req:Request, res:Response):Promise<void> => {
    const user = await get_agent_by_id(req.params.id);
    if (!user) {
       res.status(404).json({message:`No user found. with id of ${req.params.id}`});
    } else{
      res.status(200).json(user);
    }
  })
);

export default router;
