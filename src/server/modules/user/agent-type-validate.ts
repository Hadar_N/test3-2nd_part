import Ajv, {JTDSchemaType} from "ajv/dist/jtd"
import {Place, AgentInput} from "../../utils/index"
const ajv = new Ajv()
  
const place_schema: JTDSchemaType<Place> = {
    // type: "object",
    properties: {
        name: {type: "string"},
        hits: {type: "int32"},
        agent: {type: "int32"}
    },
    // required: ["name","hits","agentId"],
    // additionalProperties: false,
}

const agent_schema: JTDSchemaType<AgentInput> = {
    properties: {
        name: {type: "string"},
        type: {type: "string"},
        status: {type: "string"},
    },
    optionalProperties: {
        owner: {type: "string"}
    }
    // optionalProperties: {
    //     required: ["name","hits","agentId"]
    // }
}

export const validate_place = ajv.compile(place_schema)
export const validate_agent = ajv.compile(agent_schema)